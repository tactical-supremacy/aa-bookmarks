# Bookmarks for Alliance Auth

Bookmarks plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth/).

![License](https://img.shields.io/badge/license-MIT-green)
![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)

![python](https://img.shields.io/badge/python-3.8-informational)
![python](https://img.shields.io/badge/python-3.9-informational)
![python](https://img.shields.io/badge/python-3.10-informational)
![python](https://img.shields.io/badge/python-3.11-informational)

![django-4.0](https://img.shields.io/badge/django-4.0-informational)

Inspired by people who cant put a fucking expiry on their goddamn bookmarks

## Features

- Bookmarks Manager
  - Copy/Paste a Shared Folder to generate/update django models for bookmarks

## Planned Features

- Bookmarks Manager
  - Copy/Paste a Shared Folder to generate/update django models for bookmarks
  - Highlight faulty bookmarks, no expiry set for wormholes etc.
  - Issue invoices to repeat offenders

## Installation

### Step 1 - Django Eve Universe

Bookmarks is an App for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth/), Please make sure you have this installed. Bookmarks is not a standalone Django Application

Bookmarks needs the App [django-eveuniverse](https://gitlab.com/ErikKalkoken/django-eveuniverse) to function. Please make sure it is installed before continuing.

### Step 2 - Install app

```shell
pip install aa-Bookmarks
```

### Step 3 - Configure Auth settings

Configure your Auth settings (`local.py`) as follows:

- Add `'Bookmarks'` to `INSTALLED_APPS`
- Add below lines to your settings file:

```python
## Settings for AA-Bookmarks
## Idk probably some stuff to go here

```

### Step 4 - Maintain Alliance Auth

- Run migrations `python manage.py migrate`
- Gather your staticfiles `python manage.py collectstatic`
- Restart your project `supervisorctl restart myauth:`

### Step 5 - Pre-Load Django-EveUniverse

- `python manage.py eveuniverse_load_data map` This will load Regions, Constellations and Solar Systems

## Permissions

| Perm | Admin Site  | Perm | Description |
| --- | --- | --- | --- |
|||||

## Settings

| Name | Description | Default |
| --- | --- | --- |
||||

## Third Party Integrations

Integration with invoices planned

## Contributing

Make sure you have signed the [License Agreement](https://developers.eveonline.com/resource/license-agreement) by logging in at <https://developers.eveonline.com> before submitting any pull requests. All bug fixes or features must not include extra superfluous formatting changes.
