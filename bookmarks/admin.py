from django.contrib import admin

from allianceauth.services.hooks import get_extension_logger

from .models import AccessControlList, Bookmark, SharedFolder, SubFolder

logger = get_extension_logger(__name__)


@admin.register(SharedFolder)
class SharedFolderAdmin(admin.ModelAdmin):
    pass


@admin.register(SubFolder)
class SubFolderAdmin(admin.ModelAdmin):
    pass


@admin.register(Bookmark)
class BookmarkAdmin(admin.ModelAdmin):
    pass


@admin.register(AccessControlList)
class AccessControlListAdmin(admin.ModelAdmin):
    pass
