from django import forms

from bookmarks.models import SharedFolder


class SharedFolderForm(forms.ModelForm):

    class Meta:
        model = SharedFolder
        fields = ("name",)


class PasteForm(forms.Form):
    pass
