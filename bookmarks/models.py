from eveuniverse.models import EveSolarSystem

from django.db import models
from django.utils.translation import gettext_lazy as _

from allianceauth.eveonline.models import EveCharacter

# Self note https://esi.evetech.net/ui/?version=legacy#/Bookmarks


class General(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False
        default_permissions = ()
        permissions = (
            ("basic_bookmarks", "Can access the Bookmarks App"),
        )


class AccessControlList(models.Model):
    """An ACL"""

    name = models.CharField(
        _("Name"), max_length=50)


class SharedFolder(models.Model):
    """A Shared Bookmark Folder"""

    name = models.CharField(
        _("Folder Name"), max_length=50)

    # Maybe
    admin_acl = models.ForeignKey(
        AccessControlList,
        verbose_name=_("Admin Access"),
        on_delete=models.SET_NULL, null=True, blank=True, related_name="+")
    manage_acl = models.ForeignKey(
        AccessControlList,
        verbose_name=_("Manage Access"),
        on_delete=models.SET_NULL, null=True, blank=True, related_name="+")
    use_acl = models.ForeignKey(
        AccessControlList,
        verbose_name=_("Use Access"),
        on_delete=models.SET_NULL, null=True, blank=True, related_name="+")
    view_acl = models.ForeignKey(
        AccessControlList,
        verbose_name=_("View Access"),
        on_delete=models.SET_NULL, null=True, blank=True, related_name="+")

    class Meta:
        verbose_name = _("Shared Folder")
        verbose_name_plural = _("Shared Folders")

    def __str__(self):
        return self.name

    def locations(self):
        return 0

    def subfolders(self):
        return 0


class SubFolder(models.Model):
    """A Shared Bookmark Folder"""

    shared_folder = models.ForeignKey(
        SharedFolder, verbose_name=_("Shared Folder"), on_delete=models.CASCADE)
    name = models.CharField(
        _("Folder Name"), max_length=50)
    creator = models.CharField(
        _("Creator"), max_length=50)


class Bookmark(models.Model):
    """An EVE Bookmark"""
    shared_folder = models.ForeignKey(
        SharedFolder, verbose_name=_("Shared Folder"), on_delete=models.CASCADE)
    sub_folder = models.ForeignKey(
        SubFolder, verbose_name=_("Subfolder"), on_delete=models.CASCADE)
    label = models.CharField(
        _("Label"),
        max_length=50)
    BOOKMARK_TYPE_CHOICES = [
        ('Coordinate', 'Coordinate')]
    type = models.CharField(
        _("Type"),
        choices=BOOKMARK_TYPE_CHOICES,
        max_length=50)
    solar_system = models.ForeignKey(
        EveSolarSystem,
        verbose_name=_("Solar System"),
        on_delete=models.CASCADE,
        related_name="+")
    date = models.DateTimeField(
        _("Created"), auto_now=False, auto_now_add=False)
    expiry = models.DateTimeField(
        _("Expires"), auto_now=False, auto_now_add=False)
    creator = models.CharField(
        _("Creator"), max_length=50)

    # Cache Fields
    signature = models.CharField(
        _("Signature"), max_length=50,
        help_text="A Signature was detected in this bookmarks Label")
    creator_character = models.ForeignKey(
        EveCharacter,
        verbose_name=_("Creator Character"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True)

    class Meta:
        verbose_name = _("Bookmark")
        verbose_name_plural = _("Bookmarks")

    def __str__(self):
        return self.label
