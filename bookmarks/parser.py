import csv
import re

from eveuniverse.models import EveSolarSystem

from allianceauth.eveonline.models import EveCharacter
from allianceauth.services.hooks import get_extension_logger

from .models import Bookmark, SubFolder

logger = get_extension_logger(__name__)


def parse_locations_window(clipboard, shared_folder, initial_subfolder=None):
    """
    parse_locations_window _summary_

    :param clipboard: _description_
    :type clipboard: _type_
    :param shared_folder: _description_
    :type shared_folder: _type_
    :param initial_subfolder: _description_, defaults to None
    :type initial_subfolder: _type_, optional
    :return: _description_
    :rtype: _type_
    """
    tsv = csv.reader(clipboard, delimiter="\t")
    parsed_subfolders = []
    parsed_bookmarks = []
    current_subfolder = initial_subfolder

    for line in tsv:
        if line == "No Item":
            pass
        if line.__len__() == 1:
            # must be a folder, more checks should be used?
            current_subfolder = locations_window_subfolder(line, shared_folder)
        if line.__len__() > 8:
            parsed_bookmarks.append(
                locations_window_bookmark(line, current_subfolder))
        else:
            pass
    return parsed_subfolders, parsed_bookmarks


def parse_locations_window_popout(clipboard, shared_folder):
    """
    parse_locations_window_popout _summary_

    :param clipboard: _description_
    :type clipboard: _type_
    :return: _description_
    :rtype: _type_
    """
    # This window is special, it does not include folder names
    # Use this only for bookmark cleanup / bulk creation
    tsv = csv.reader(clipboard, delimiter="\t")
    parsed_bookmarks = []

    for line in tsv:
        if line == "No Item":
            pass
        if line.__len__() == 9:
            parsed_bookmarks.append(locations_window_bookmark(line))
        else:
            pass
    return parsed_bookmarks


def locations_window_bookmark(line, subfolder_if_known=None):
    # Cargo Container	Coordinate	31	J-CIJV	38G6-L	Pure Blind	2023.03.14 11:56	-	Cpt Enzo
    bookmark = Bookmark
    bookmark.label = line[0]
    bookmark.type = line[1]
    bookmark.solar_system, fetched = EveSolarSystem.objects.get_or_create_esi(
        id=line[3])
    bookmark.date = line[6]
    bookmark.expiry = line[7]
    bookmark.creator = line[8]
    if subfolder_if_known is not None:
        bookmark.sub_folder = subfolder_if_known
    try:
        bookmark.creator_character = EveCharacter.objects.get(
            name=bookmark.creator)
    except Exception:
        pass

    return bookmark


def locations_window_subfolder(line, shared_folder) -> SubFolder | None:
    # Temp Bookmarks (Fawlty7)
    try:
        subfolder, created = SubFolder.objects.get_or_create(
            name=line[0],
            creator=re.split(r'[()]', line)[-1],
            shared_folder=shared_folder
        )
    except Exception as e:
        logger.exception(e)
        subfolder = None

    return subfolder
