from django.contrib.auth.decorators import login_required, permission_required
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from bookmarks.forms import PasteForm, SharedFolderForm
from bookmarks.models import SharedFolder
from bookmarks.parser import (
    parse_locations_window, parse_locations_window_popout,
)


@login_required
@permission_required("bookmarks.basic_access")
def index(request: WSGIRequest) -> HttpResponse:
    """
    Index view
    :param request:
    :return:
    """
    sharedfolders = SharedFolder.objects.all()

    context = {
        "sharedfolders": sharedfolders,
    }

    return render(request, "bookmarks/index.html", context)


@login_required
def sharedfolder_add(request):
    if request.method == "POST":
        form = SharedFolderForm(request.POST)
        if form.is_valid():
            SharedFolder.objects.create(form.cleaned_data["Name"])
            return HttpResponseRedirect("/thanks/")
    else:
        form = SharedFolderForm()

    return render(request, "bookmarks/sharedfolder_add.html", {"form": form})


@login_required
def add_paste(request):
    # ctx = {}
    if request.method == "POST":
        form = SharedFolderForm(request.POST)
        if form.is_valid():
            if form.fields['is_popout'] is True:
                parse_locations_window_popout(form.fields["clipboard"], form.fields["shared_folder"])
            else:
                parse_locations_window(form.fields["clipboard"], form.fields["shared_folder"])
    else:
        form = PasteForm()

    return render(request, "bookmarks/paste.html", {"form": form})


# ask for target Shared Folder
# ask for top folder assume, since this isnt in copy paste
