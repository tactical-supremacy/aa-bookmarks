from django.utils.translation import gettext_lazy as _

from allianceauth import hooks
from allianceauth.menu.hooks import MenuItemHook
from allianceauth.services.hooks import UrlHook

from . import urls


class BookmarksMenuItem(MenuItemHook):
    """This class ensures only authorized users will see the menu entry"""

    def __init__(self):
        # setup menu entry for sidebar
        MenuItemHook.__init__(
            self,
            _("Bookmarks"),
            "fas fa-bookmark fa-fw",
            "bookmarks:index",
            navactive=["bookmarks:index"],
        )

    def render(self, request):
        if request.user.has_perm("bookmarks.basic_access"):
            return MenuItemHook.render(self, request)
        return ""


@hooks.register("menu_item_hook")
def register_menu():
    return BookmarksMenuItem()


@hooks.register("url_hook")
def register_urls():
    return UrlHook(urls, "bookmarks", r"^bookmarks/")


@hooks.register('discord_cogs_hook')
def register_cogs():
    return ["bookmarks.cogs.bookmarks"]
