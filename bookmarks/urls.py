from django.urls import path

from . import views

app_name: str = "bookmarks"

urlpatterns = [
    path("", views.index, name="index"),
]
