# Generated by Django 4.0.10 on 2023-05-16 03:38

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sharedfolder',
            name='admin_acl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Admin Access'),
        ),
        migrations.AlterField(
            model_name='sharedfolder',
            name='manage_acl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Manage Access'),
        ),
        migrations.AlterField(
            model_name='sharedfolder',
            name='use_acl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Use Access'),
        ),
        migrations.AlterField(
            model_name='sharedfolder',
            name='view_acl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='View Access'),
        ),
    ]
