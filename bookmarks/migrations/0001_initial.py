# Generated by Django 4.0.10 on 2023-05-08 05:24

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('eveuniverse', '0007_evetype_description'),
        ('eveonline', '0017_alliance_and_corp_names_are_not_unique'),
    ]

    operations = [
        migrations.CreateModel(
            name='General',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'permissions': (('basic_bookmarks', 'Can access the Bookmarks App'),),
                'managed': False,
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='AccessControlList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='')),
            ],
        ),
        migrations.CreateModel(
            name='SharedFolder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Folder Name')),
                ('admin_acl', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Admin Access')),
                ('manage_acl', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Manage Access')),
                ('use_acl', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='Use Access')),
                ('view_acl', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='bookmarks.accesscontrollist', verbose_name='View Access')),
            ],
            options={
                'verbose_name': 'Shared Folder',
                'verbose_name_plural': 'Shared Folders',
            },
        ),
        migrations.CreateModel(
            name='SubFolder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Folder Name')),
                ('creator', models.CharField(max_length=50, verbose_name='Creator')),
                ('shared_folder', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bookmarks.sharedfolder', verbose_name='')),
            ],
        ),
        migrations.CreateModel(
            name='Bookmark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.CharField(max_length=50, verbose_name='Label')),
                ('type', models.CharField(choices=[('Coordinate', 'Coordinate')], max_length=50, verbose_name='Type')),
                ('date', models.DateTimeField(verbose_name='Created')),
                ('expiry', models.DateTimeField(verbose_name='Expires')),
                ('creator', models.CharField(max_length=50, verbose_name='Creator')),
                ('signature', models.CharField(help_text='A Signature was detected in this bookmarks Label', max_length=50, verbose_name='Signature')),
                ('creator_character', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='eveonline.evecharacter', verbose_name='Creator Character')),
                ('shared_folder', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bookmarks.sharedfolder', verbose_name='')),
                ('solar_system', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='eveuniverse.evesolarsystem', verbose_name='Solar System')),
                ('sub_folder', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bookmarks.subfolder', verbose_name='')),
            ],
            options={
                'verbose_name': 'Bookmark',
                'verbose_name_plural': 'Bookmarks',
            },
        ),
    ]
